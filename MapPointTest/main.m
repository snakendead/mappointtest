//
//  main.m
//  MapPointTest
//
//  Created by developer2 on 12.09.16.
//  Copyright © 2016 developer2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
