//
//  TimerMarker.h
//  MapPointTest
//
//  Created by developer2 on 12.09.16.
//  Copyright © 2016 developer2. All rights reserved.
//

#import <GoogleMaps/GoogleMaps.h>

@interface TimerMarker : GMSMarker

@property (nonatomic) NSDate *startDate;

- (void)updateTitle;

@end
