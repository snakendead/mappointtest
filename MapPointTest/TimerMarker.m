//
//  TimerMarker.m
//  MapPointTest
//
//  Created by developer2 on 12.09.16.
//  Copyright © 2016 developer2. All rights reserved.
//

#import "TimerMarker.h"

@implementation TimerMarker

- (void)updateTitle {
    self.title = [self stringFromTimeInterval:fabs([self.startDate timeIntervalSinceDate:[NSDate date]])];
}

-(instancetype) init {
    self = [super init];
    if (self) {
        self.startDate = [NSDate date];
        self.tracksInfoWindowChanges = true;
        self.tracksViewChanges = true;
        [self updateTitle];
    }
    return self;
}

- (NSString *)stringFromTimeInterval:(NSTimeInterval)interval {
    NSInteger ti = (NSInteger)interval;
    NSInteger seconds = ti % 60;
    NSInteger minutes = (ti / 60) % 60;
    NSInteger hours = (ti / 3600);
    return hours != 0 ? [NSString stringWithFormat:@"%02ld h", (long)hours] : [NSString stringWithFormat:@"%02ld:%02ld", (long)minutes, (long)seconds] ;
}

@end
