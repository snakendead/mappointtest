//
//  ViewController.m
//  MapPointTest
//
//  Created by developer2 on 12.09.16.
//  Copyright © 2016 developer2. All rights reserved.
//

#import "ViewController.h"
#import "TimerMarker.h"
@import GoogleMaps;

@interface ViewController ()

@property (nonatomic) GMSMapView *mapView_;
@property (weak, nonatomic) IBOutlet UIButton *startButton;
@property (weak, nonatomic) IBOutlet UIButton *endButton;
@property (nonatomic) NSMutableSet <TimerMarker *> *markers;
@property (nonatomic) NSTimer *timer;

@end

@implementation ViewController

- (void)viewDidLoad {
    self.markers = [[NSMutableSet alloc] init];
    // Create a GMSCameraPosition that tells the map to display the
    // coordinate -33.86,151.20 at zoom level 6.
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:-33.86
                                                            longitude:151.20
                                                                 zoom:6];
    self.mapView_ = [GMSMapView mapWithFrame:CGRectZero camera:camera];
    self.mapView_.myLocationEnabled = YES;
    [self.view addSubview:self.mapView_];
    [self.view sendSubviewToBack:self.mapView_];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    self.mapView_.frame = self.view.bounds;
}

- (IBAction)startButtonAction:(UIButton *)sender {
    TimerMarker *marker = [[TimerMarker alloc] init];
    marker.position = [self.mapView_.camera target];
    marker.map = self.mapView_;
    [self.markers addObject:marker];
    self.mapView_.selectedMarker = marker;
    [self startTimer];
}

- (IBAction)endButtonAction:(UIButton *)sender {
    [self stopTimer];
}

- (void)startTimer {
    if (!self.timer) {
        self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTimer) userInfo:nil repeats:true];
        [self.timer fire];
    }
}

- (void)stopTimer {
    [self.timer invalidate];
    self.timer = nil;
    for (TimerMarker *marker in self.markers) {
        marker.snippet = @"Marker finished.";
    }
    [self updateTimer];
    [self.markers removeAllObjects];
}

- (void)updateTimer {
    for (TimerMarker *marker in self.markers) {
        [marker updateTitle];
    }
    if (self.mapView_.selectedMarker) {
        GMSMarker *marker = self.mapView_.selectedMarker;
        self.mapView_.selectedMarker = nil;
        self.mapView_.selectedMarker = marker;
    }
}
@end
